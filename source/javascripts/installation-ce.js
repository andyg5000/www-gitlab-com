var getUrlParameter = function getUrlParameter(param) {
  var pageURL = decodeURIComponent(window.location.search.substring(1)),
      urlParams = pageURL.split('&');

  for (var i = 0; i < urlParams.length; i++) {
    var parameterName = urlParams[i].split('=');

    if (parameterName[0] === param) {
      return parameterName[1] === undefined ? true : parameterName[1];
    }
  }
};

$(function() {
  var internalNavigationEvent = 'onpopstate' in window ? 'popstate' : 'hashchange';
  var distro = getUrlParameter('distro');

  if (distro) {
    document.getElementById("ce_link").href = "/installation/#" + distro + "?version=ce";
  }
});
